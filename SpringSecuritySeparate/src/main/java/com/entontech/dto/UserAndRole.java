package com.entontech.dto;

import lombok.Data;

/**
 * @author liha
 * @version 1.0
 * @date 2022/3/30 14:35
 * @description 用户表和角色表的联查实体类
 */
@Data
public class UserAndRole {

    /**
     * 用户的昵称
     * */
    private String userName;

    /**
     *  用户的角色身份，等于与一个系统可能拥有superAdmin、admin、user
     * */
    private String roleCode;

    /**
     *  角色名字
     * */
    private String roleName;

}
