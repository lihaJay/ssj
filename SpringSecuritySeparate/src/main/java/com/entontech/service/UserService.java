package com.entontech.service;

import com.entontech.dto.UserAndRole;
import com.entontech.vo.UserInfo;

/**
 * @author liha
 * @version 1.0
 * @date 2022/3/30 14:08
 * @description
 */
public interface UserService {
    UserAndRole getUserInfoByAccount(String memberAccountByJwtToken);
}
