package com.entontech.service.imple;

import com.entontech.dto.UserAndRole;
import com.entontech.vo.UserInfo;
import com.entontech.mapper.SysUserMapper;
import com.entontech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liha
 * @version 1.0
 * @date 2022/3/30 14:08
 * @description
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * 注入用户的mapper层接口，查询数据库
     * */
    @Autowired
    private SysUserMapper userMapper;

    @Override
    public UserAndRole getUserInfoByAccount(String memberAccountByJwtToken) {
        UserAndRole userAndRole = userMapper.getUserInfoByAccount(memberAccountByJwtToken);
        return userAndRole;
    }
}
