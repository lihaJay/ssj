package com.entontech;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author com.liha
 * @version 1.0
 * @date 2022/3/25 10:26
 * @description
 */
@SpringBootApplication
@MapperScan("com.entontech.mapper")
public class Application {


    public static void main(String[] args) {

        SpringApplication.run(Application.class,args);

    }
}
