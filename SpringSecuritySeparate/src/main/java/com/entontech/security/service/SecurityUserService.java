package com.entontech.security.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author com.liha
 * @version 1.0
 * @date 2022/3/25 14:20
 * @description
 */
public interface SecurityUserService extends UserDetailsService {

}
