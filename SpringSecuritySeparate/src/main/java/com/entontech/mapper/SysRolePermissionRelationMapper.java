package com.entontech.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysRolePermissionRelation;

/**
 * <p>
 * 角色-权限关联关系表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
public interface SysRolePermissionRelationMapper extends BaseMapper<SysRolePermissionRelation> {

}
