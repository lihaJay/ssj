package com.entontech.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
