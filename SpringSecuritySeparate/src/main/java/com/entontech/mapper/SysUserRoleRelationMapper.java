package com.entontech.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysUserRoleRelation;

/**
 * <p>
 * 用户角色关联关系表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
public interface SysUserRoleRelationMapper extends BaseMapper<SysUserRoleRelation> {

}
