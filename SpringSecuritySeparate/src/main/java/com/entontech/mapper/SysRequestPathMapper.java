package com.entontech.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysRequestPath;

/**
 * <p>
 * 请求路径 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
public interface SysRequestPathMapper extends BaseMapper<SysRequestPath> {

}
