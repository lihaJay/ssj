package com.entontech.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
@Repository
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    /**
     * 找到当前用户的权限
     * */
    List<SysPermission> getUserRolesByUserId(Integer id);

    List<SysPermission> selectListByPath(@Param("requestUrl") String requestUrl);
}
