package com.entontech.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.dto.UserAndRole;
import com.entontech.vo.UserInfo;
import com.entontech.entiry.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {


    UserAndRole getUserInfoByAccount(@Param("memberAccountByJwtToken") String memberAccountByJwtToken);
}
