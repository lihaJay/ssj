package com.entontech.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entontech.entiry.SysRequestPathPermissionRelation;

/**
 * <p>
 * 路径权限关联表 Mapper 接口
 * </p>
 *
 * @author com.liha
 * @since 2022-03-28
 */
public interface SysRequestPathPermissionRelationMapper extends BaseMapper<SysRequestPathPermissionRelation> {

}
