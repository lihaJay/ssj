package com.entontech.vo;

import lombok.Data;

/**
 * @author liha
 * @version 1.0
 * @date 2022/3/30 14:19
 * @description 用户信息  用于返回给前段的实体类
 */
@Data
public class UserInfo {


    /**
     * 用户的昵称
     * */
    private String userName;

    /**
     *  用户的角色身份，等于与一个系统可能拥有superAdmin、admin、user
     * */
    private String[] roleCodes;

    /**
     *  角色名字
     * */
    private String roleName;

    /**
     * 用户的头像，数据库暂时没有，用死的
     * */
    private String avatar;




}
