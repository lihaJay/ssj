package com.entontech.controller;

import com.entontech.common.result.JsonResult;
import com.entontech.common.result.ResultTool;
import com.entontech.common.utils.JwtUtils;
import com.entontech.dto.UserAndRole;
import com.entontech.vo.UserInfo;
import com.entontech.security.service.SecurityUserService;
import com.entontech.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author com.liha
 * @version 1.0
 * @date 2022/3/25 10:29
 * @description 用户相关的controller
 */
@RestController
@CrossOrigin
public class UserController {

    /**
     * 注入security的UserDetailsService
     *  因为可以通过他获取到一些数据
     * */
    @Autowired
    private SecurityUserService securityUserService;


    /**
     * 注入关于UserSerivce
     * */
    @Autowired
    private UserService userService;


    @GetMapping("/getUser")
    public JsonResult getUser(){
        return ResultTool.success();
    }

    @GetMapping("/deleteUser")
    public JsonResult deleteUser(){
        return ResultTool.success();
    }

    @GetMapping("test")
    public JsonResult test(){
        Map<String,String> map = new HashMap<>();
        map.put("hell","wolrd");
        return ResultTool.success(map);
    }


    /**
     * 根据security中userService获取到用户的信息
     *  包括：
     *      角色
     *      username
     *      还有头像，但是头像暂时没得，直接用默认的
     * */
    @GetMapping("info")
    public JsonResult getInfo(HttpServletRequest httpServletRequest){

        // 要明白能进到这里来，已经是通过过滤器链了，所以是安全的
        // 先通过JWT获取到用户的account
        String memberAccountByJwtToken = JwtUtils.getMemberAccountByJwtToken(httpServletRequest);

        // todo 一个联查   根据用户的account查询到用户的username和角色信息 finfish
        UserAndRole userAndRole = userService.getUserInfoByAccount(memberAccountByJwtToken);

        UserInfo userInfo = new UserInfo();
        // 实体类的转换
        BeanUtils.copyProperties(userAndRole,userInfo);

        userInfo.setRoleCodes(new String[]{userAndRole.getRoleCode()});
        // 添加死的头像
        userInfo.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return ResultTool.success(userInfo);
    }


//    @GetMapping("info")
//    public JsonResult getInfo(){
//        HashMap<String,String> hashMap = new HashMap<>();
//        hashMap.put("roles","[admin]");
//        hashMap.put("name","admin");
//        hashMap.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
//        return ResultTool.success(hashMap);
//    }
}
