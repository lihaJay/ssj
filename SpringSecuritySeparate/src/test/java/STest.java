import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Author liha
 * @Date 2022-03-30 22:27
 * 李哈YYDS
 */
public class STest {

    @Test
    public void test(){
        String password = "liha";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(password);
        System.out.println(encode);
    }
}
